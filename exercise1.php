<?php
    /**
    * Check string.
    *
    * @param string $string
    * @return bolean
    */
    function checkValidString($string) 
    {
        $length = strlen($string);
        $pos1 = strpos($string, 'after');
        $pos2 = strpos($string, 'before');
        if (empty($string) || ($length > 50 && $pos1 === false) || ($pos1 === false && $pos2 !== false)) {
            return true;
        } 
        return false;
    }
    $text1 = file_get_contents('file1.txt');
    $text2 = file_get_contents('file2.txt');
    if (checkValidString($text1)) {
        echo 'Chuỗi hợp lệ <br>';
    } else {
        echo 'Chuỗi không hợp lệ <br>';
    } 
    if (checkValidString($text2)) {
        echo 'Chuỗi hợp lệ <br>';
    } else {
        echo 'Chuỗi không hợp lệ <br>';
    }
?>
